<ul>
    @foreach($childs as $child)
        <li>
            @if($child->node === true)
                {{ $child->name }}

                <p class="key">
                    {{ $child->key }}
                </p>

                <a class="translationNamePlus">
                    <i class="fas fa-plus"></i>
                </a>
            @else
                {{ $child->name }}

                <p>
                    @isset($child->translationName)
                        PL - {{ $child->translationName->name_pl }}
                        <br/>
                        EN - {{ $child->translationName->name_en }}
                        <br/>
                        RU - {{ $child->translationName->name_ru }}
                    @endisset
                </p>
            @endif

            @if(count($child->translations))
                @include('includes._child', [
                    'childs' => $child->translations
                ])
            @endif
        </li>
    @endforeach
</ul>
