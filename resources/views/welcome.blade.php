<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <style>
        .add-translation {
            position: fixed;
            right: 0;
        }

        .translationNamePlus {
            color: blue !important;
        }

        .key {
            display: none;
        }
    </style>
</head>
<body>
<div class="row">
    <div class="col-6">
        <h3>Category List</h3>
        <ul id="tree1">
            @foreach($translations as $translation)
                <li>
                    @if($translation->node === true)
                        {{ $translation->name }}

                        <p class="key">
                            {{ $translation->key }}
                        </p>

                        <a class="translationNamePlus">
                            <i class="fas fa-plus"></i>
                        </a>
                    @else
                        {{ $translation->name }}

                        <p>
                            @isset($translation->translationName)
                                PL - {{ $translation->translationName->name_pl }}
                                <br/>
                                EN - {{ $translation->translationName->name_en }}
                                <br/>
                                RU - {{ $translation->translationName->name_ru }}
                            @endisset
                        </p>
                    @endif

                    @if(count($translation->translations))
                        @include('includes._child', [
                            'childs' => $translation->translations
                        ])
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
    <div class="col-6 add-translation">
        <h3>Add New Translate</h3>

        <form method="POST" action="{{ route('translations.create') }}">
            @csrf

            <div class="form-group">
                <label for="translation_key">Translation Key</label>
                <textarea class="form-control" name="translation_key" id="translation_key" cols="5" rows="5" required></textarea>
                <small id="translation_key_help" class="form-text text-muted">Here you can write translation key.</small>
            </div>
            <div class="form-group">
                <label for="name_pl">Translation Value</label>
                <input type="text" class="form-control" id="name_pl" name="name_pl" aria-describedby="name_pl_help" placeholder="Translation Value" required>
                <small id="name_pl_help" class="form-text text-muted">Here you can write translation value in polish language.</small>
            </div>

            <button type="submit" class="btn btn-primary">
                Add
            </button>
        </form>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
<script>
    $('.translationNamePlus').on('click', function (event) {
        $('#translation_key').val($(this).closest('li').find('.key').first().text().trim());
    });
</script>
</body>
</html>
