<?php

namespace App\Traits;

use App\Jobs\TranslateJob;

trait Translatable {
    protected static function languages() {
        return [
            'en',
            'pl',
            'ru'
        ];
    }

    public function translate($source, $expression, $model, $field) {
        foreach (self::languages() as $language) {
            if ($source !== $language) {
                TranslateJob::dispatchNow($source, $language, $expression, $model, $field . '_' . $language);
            } else {
                $model->name_pl = $expression;
                $model->save();
            }
        }
    }
}
