<?php

namespace App\Http\Controllers;

use App\Traits\Translatable;
use App\Translation;
use App\TranslationName;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TranslationController extends Controller {
    use Translatable;

    private $basePath,
            $pl,
            $en,
            $ru;

    public function __construct() {
        $this->basePath = __DIR__ . '/../../../../stowarzyszenie/front/src/assets/i18n/';

        $this->pl = json_decode(file_get_contents($this->basePath . 'pl.json'), true);
        $this->en = json_decode(file_get_contents($this->basePath . 'en.json'), true);
        $this->ru = json_decode(file_get_contents($this->basePath . 'ru.json'), true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        Translation::truncate();

        factory(Translation::class)->create([
            'name' => 'STOWARZYSZENIE',
            'key' => 'STOWARZYSZENIE'
        ]);

        $this->displayArrayRecursively($this->pl, $this->pl, $this->en, $this->ru);

        $translations = Translation::with(['translations', 'translationName'])
            ->get()
            ->sortBy('key');

        return view('welcome', compact('translations'));
    }

    private function displayArrayRecursively($array, $pl, $en, $ru, $translation_key = '') {
        if ($array) {
            $this->addTranslation($pl, $en, $ru, $translation_key);

            foreach ($array as $key => $value) {
                if (is_array($value)) {
                    $this->displayArrayRecursively(
                        $value,
                        $pl,
                        $en,
                        $ru,
                        $translation_key . ($translation_key === '' ? '' : '.') . $key
                    );
                } else {
                    $this->addTranslation($pl, $en, $ru,$translation_key . '.' . $key, false);
                }
            }
        }
    }

    private function addTranslation($pl, $en, $ru, $translation_key, $node = true) {
        if ($translation_key === '' || $translation_key === 'STOWARZYSZENIE') {
            return;
        }

        $translationDividedOriginal = explode('.', $translation_key);
        $translationDivided = $translationDividedOriginal;
        $lastTranslationDivided = array_pop($translationDivided);

        $translation = new Translation();
        $translation->name = $lastTranslationDivided;
        $translation->key = $translation_key;
        $translation->node = $node;
        $translation->translation_id = Translation::where(
            'key', implode('.', $translationDivided)
        )->first()->id;
        $translation->save();

        if (!$node) {
            foreach ($translationDividedOriginal as $key) {
                $pl = $pl[$key];
                $en = $en[$key];
                $ru = $ru[$key];
            }

            $translationName = new TranslationName();
            $translationName->name_pl = $pl;
            $translationName->name_en = $en;
            $translationName->name_ru = $ru;
            $translationName->translation_id = $translation->id;
            $translationName->save();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request) {
        $input = $request->input();

        if ($this->translationExists($input['translation_key'])) {
            return $this->index();
        }

        $translationDivided = explode('.', $input['translation_key']);
        $translationDividedShifted = [];

        while (!$this->translationExists(implode('.', $translationDivided))) {
            array_unshift($translationDividedShifted, array_pop($translationDivided));
        }

        $translation = new Translation();
        $translation->name = $translationDividedShifted[0];
        $translation->key = implode('.', $translationDivided) . '.' . $translationDividedShifted[0];
        $translation->node = false;
        $translation->translation_id = Translation::where(
            'key', implode('.', $translationDivided)
        )->first()->id;
        $translation->save();

        $translationName = new TranslationName();
        $this->translate(
            'pl',
            $input['name_pl'],
            $translationName,
            'name'
        );
        $translationName->translation_id = $translation->id;
        $translationName->save();

        $this->saveToJSON(
            $translationDivided,
            $translationDividedShifted,
            $translationName->id
        );

        return $this->index();
    }

    function translationExists($translation) {
        return Translation::where('key', $translation)->exists();
    }

    function saveToJSON($translationDivided, $translationDividedShifted, $translationNameId) {
        $translationName = TranslationName::where('id', $translationNameId)
            ->first();

        $this->changeTranslations(
            $translationDivided,
            $translationDividedShifted,
            $translationName,
            $this->pl,
            $this->en,
            $this->ru
        );

        $this->saveToFile($this->pl,'pl.json');
        $this->saveToFile($this->en, 'en.json');
        $this->saveToFile($this->ru, 'ru.json');
    }

    function saveToFile($translations, $fileName) {
        $file = fopen($this->basePath . $fileName, 'w');
        fwrite(
            $file,
            json_encode(
                $translations,
                JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
            )
        );
        fclose($file);
    }

    function changeTranslations(
        $translationDivided,
        $translationDividedShifted,
        $translationName,
        &$pl,
        &$en,
        &$ru
    ) {
        $translationKey = array_shift($translationDivided);

        if ($translationKey !== null) {
            return $this->changeTranslations(
                $translationDivided,
                $translationDividedShifted,
                $translationName,
                $pl[$translationKey],
                $en[$translationKey],
                $ru[$translationKey]
            );
        }

        if (count($translationDividedShifted) > 1) {
            $pl[$translationDividedShifted[0]] = [];
            $en[$translationDividedShifted[0]] = [];
            $ru[$translationDividedShifted[0]] = [];

            $translationKey = array_shift($translationDividedShifted);

            return $this->changeTranslations(
                $translationDivided,
                $translationDividedShifted,
                $translationName,
                $pl[$translationKey],
                $en[$translationKey],
                $ru[$translationKey]
            );
        }

        $pl[$translationDividedShifted[0]] = $translationName->name_pl;
        $en[$translationDividedShifted[0]] = $translationName->name_en;
        $ru[$translationDividedShifted[0]] = $translationName->name_ru;

        return $pl;
    }
}
