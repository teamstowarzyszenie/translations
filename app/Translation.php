<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    protected $fillable = [
        'key',
        'node',
        'translation_id'
    ];

    public function translationName() {
        return $this->hasOne(TranslationName::class);
    }

    public function translations() {
        return $this->hasMany(Translation::class);
    }
}
