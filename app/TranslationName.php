<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TranslationName extends Model
{
    protected $fillable = [
        'name_pl',
        'name_en',
        'name_ru',
        'translation_id',
    ];

    public function translation() {
        return $this->belongsTo(Translation::class);
    }
}
