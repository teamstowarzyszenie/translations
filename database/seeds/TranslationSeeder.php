<?php

use App\Translation;
use Illuminate\Database\Seeder;

class TranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Translation::class)->create([
            'key' => 'STOWARZYSZENIE'
        ]);
    }
}
