<?php

/** @var Factory $factory */

use App\Model;
use App\Translation;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Translation::class, function (Faker $faker, $params) {
    return [
        'name' => $params['name'] ?? $faker->sentences(1),
        'key' => $params['key'] ?? $faker->sentences(1)
    ];
});
